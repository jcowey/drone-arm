FROM easypi/alpine-arm as alpine

RUN apk add -U --no-cache ca-certificates

FROM scratch
ENTRYPOINT []
WORKDIR /
COPY --from=alpine /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

ENV GODEBUG=netdns=go
ENV DRONE_PLATFORM=linux/arm
ADD release/linux/arm/drone-agent /bin/

EXPOSE 3000
HEALTHCHECK CMD ["/bin/drone-agent", "ping"]

ENTRYPOINT ["/bin/drone-agent"]
